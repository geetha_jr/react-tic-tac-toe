import React from "react";
import { FaRegCircle, FaTimes, FaQuestion } from 'react-icons/fa';

const Icon = ({ name= 'empty'}) => {
  if (name === 'empty') {
    return <FaQuestion />;
  } else if (name === 'circle') {
    return <FaRegCircle />
  } else {
    return <FaTimes />
  }
}

export default Icon;