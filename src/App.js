import React, { useState } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Col, Row, Card, CardBody, Button } from "reactstrap";
import Icon from "./Icon";
import './App.css';

const arrayItems = new Array(9).fill("empty");

const App = () => {
  const [isCross, setIsCross] = useState(true);
  const [successMessage, setSuccessMessage] = useState("");

  const isGameOver = () => {
    if (arrayItems[0] !== 'empty' && arrayItems[0] === arrayItems[1]
      && arrayItems[1] === arrayItems[2]) {
      return !isCross ? "Circle Won" : "Cross Won";
    } else if (arrayItems[3] !== 'empty' && arrayItems[4] === arrayItems[3]
      && arrayItems[4] === arrayItems[5]) {
      return !isCross ? "Circle Won" : "Cross Won";
    } else if (arrayItems[6] !== 'empty' && arrayItems[6] === arrayItems[7]
      && arrayItems[7] === arrayItems[8]) {
      return !isCross ? "Circle Won" : "Cross Won";
    } else if (arrayItems[0] !== 'empty' && arrayItems[0] === arrayItems[3]
      && arrayItems[3] === arrayItems[6]) {
      return !isCross ? "Circle Won" : "Cross Won";
    } else if (arrayItems[1] !== 'empty' && arrayItems[1] === arrayItems[4]
      && arrayItems[4] === arrayItems[7]) {
      return !isCross ? "Circle Won" : "Cross Won";
    } else if (arrayItems[2] !== 'empty' && arrayItems[2] === arrayItems[5]
      && arrayItems[5] === arrayItems[8]) {
      return !isCross ? "Circle Won" : "Cross Won";
    } else if (arrayItems[0] !== 'empty' && arrayItems[0] === arrayItems[4]
      && arrayItems[4] === arrayItems[8]) {
      return !isCross ? "Circle Won" : "Cross Won";
    } else if (arrayItems[2] !== 'empty' && arrayItems[2] === arrayItems[4]
      && arrayItems[4] === arrayItems[6]) {
      return !isCross ? "Circle Won" : "Cross Won";
    } else if (arrayItems.indexOf('empty') < 0) {
      return "Match Tied";
    } else {
      return "";
    }
  }

  const changeItem = (index) => {
    if (successMessage) {
      // bring a toast
      return;
    } else if (arrayItems[index] !== 'empty') {
      // bring a toast
      return;
    }
    else if (!successMessage && isCross) {
      arrayItems[index] = "cross";
    } else {
      arrayItems[index] = "circle";
    }
    const message = isGameOver();
    if (message) {
      setSuccessMessage(message);
    }
    setIsCross(!isCross);
  }

  const reloadGame = () => {
    arrayItems.fill('empty');
    setSuccessMessage("");
  }

  return (
    <Container className="py-3">
      <Row>
        <Col md={6} className="offset-md-3">
          <div className="text-center py-3 text-success">
            {successMessage ? (
              <div>
                <h4>{successMessage}</h4>
                <Button className="btn-sm" onClick={reloadGame}>Reload</Button>
              </div>
            ) : (
              <h4>{isCross ? 'Cross\'s Turn' : 'Circle\'s Turn'}</h4>
            )}
          </div>
          <div className="grid-container">
            {arrayItems.map((item, index) => (
              <Card onClick={() => { changeItem(index) }}>
                <CardBody className="flex-center">
                  <Icon name={item} />
                </CardBody>
              </Card>
            ))}
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default App;